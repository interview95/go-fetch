package helpers

import (
	"fmt"
	"regexp"
	"time"
)

type PatternType string

const (
	Link  PatternType = "<a[^>]*>(.*?)</a>"
	Image PatternType = "\\.jpg|\\.png"
)

// Use regex to find all <a> tags in HTML
func FindElementsByPattern(html []byte, pattern PatternType) int {
	r, _ := regexp.Compile(string(pattern))
	matches := r.FindAllString(string(html), -1)
	return len(matches)
}

// Print additional information on screen
func PrintToScreen(url string, links int, imgs int) {
	fmt.Println("\nsite: ", url)
	fmt.Println("num_links: ", links)
	fmt.Println("images: ", imgs)

	t := time.Now()
	formatted_datetime := t.Format("Mon Jan 2 2006 15:04 UTC")
	fmt.Println("last_fetch: ", formatted_datetime)
	fmt.Println("-----------")
}
