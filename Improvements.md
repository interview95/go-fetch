## Improvements
### Fetch all assets
> Archive all assets as well as html so that you load the web page locally with all assets loading properly.

To fetch all the assets from the web page, it would have been necessary to scan all the sources available recursively on the page and and download them locally.
The search would likely require a substantial computational complexity, so *Go channels* could be used to parallelize the fetch of different types of sources so to speed up
the process, since the order of the download is not important. 