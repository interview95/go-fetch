package cmd

import (
	"errors"
	"io"
	"net/http"
	"os"
	"strings"
	"time"

	t "github.com/alebaffa/fetch/helpers"
	"github.com/spf13/cobra"
)

var Metadata bool

// rootCmd represents the base command when called without any subcommands
var rootCmd = &cobra.Command{
	Long: `This command fetches the HTML page from a list of URLs and save them into files locally.
	
	Example: if fetch from https://www.google.com, then it saves the pointed HTML page into current_path/www.google.com.html`,

	RunE: func(cmd *cobra.Command, args []string) error {

		if len(args) == 0 {
			return errors.New("no links in input")
		}

		timeout := time.Duration(2 * time.Second)
		client := http.Client{
			Timeout: timeout,
		}

		for _, url := range args {
			url_without_https := strings.Split(url, "//")[1]
			resp, err := client.Get(url)

			if err != nil {
				return err
			}

			defer resp.Body.Close()

			html, err := io.ReadAll(resp.Body)
			if err != nil {
				return err
			}

			filename := url_without_https + ".html"
			error := os.WriteFile(filename, html, 0644)
			if error != nil {
				return err
			}

			numOfLinks := t.FindElementsByPattern(html, t.Link)
			numOfImgs := t.FindElementsByPattern(html, t.Image)
			if Metadata {
				t.PrintToScreen(url_without_https, numOfLinks, numOfImgs)
			}
		}
		return nil
	},
}

// Execute adds all child commands to the root command and sets flags appropriately.
// This is called by main.main(). It only needs to happen once to the rootCmd.
func Execute() {
	err := rootCmd.Execute()
	if err != nil {
		os.Exit(-1)
	}
}

func init() {
	rootCmd.Flags().BoolVarP(&Metadata, "metadata", "m", false, "Visualize additional info about the URLs fetched.")
}
