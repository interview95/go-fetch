FROM golang:latest

RUN apt update && apt install git && mkdir /app

ADD . /app

WORKDIR /app

EXPOSE 8080

RUN go build .